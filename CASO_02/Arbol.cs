﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CASO_02
{
    class Arbol
    {
        public Nodo raiz;
        public Nodo GetRaiz()
        {
            return raiz;
        }
        public void InsertarNodo(int id, string historia, string paciente)
        {
            Nodo puntero;
            Nodo padre;
            Nodo nodo = new Nodo {id = id, historia = historia, paciente = paciente };
            Console.WriteLine("Agregando {0} ...",historia);
            if (raiz != null)
            {
                puntero = raiz;
                while (true)
                {
                    padre = puntero;
                    if (id < puntero.id)
                    {
                        puntero = puntero.izquierdo;
                        if (puntero == null)
                        {
                            padre.izquierdo = nodo;
                            break;
                        }
                    }
                    else
                    {
                        puntero = puntero.derecho;
                        if (puntero == null)
                        {
                            padre.derecho = nodo;
                            break;
                        }
                    }
                }
            }
            else
            {
                raiz = nodo;
            }
        }
        public void BuscarPorID(int id)
        {
            int contador = 0;
            Nodo puntero = raiz;
            Console.WriteLine("\nBuscando Historia Clinica nro {0}",id);
            while (puntero != null)
            {
                contador += 1;
                if (puntero.id == id)
                {
                    Console.WriteLine("{0}, {1} encontrada", puntero.historia,puntero.paciente);
                    Console.WriteLine("Total de iteraciones:" + contador);
                    return;
                }
                else
                {
                    if (id > puntero.id)
                    {
                        puntero = puntero.derecho;
                    }
                    else
                    {
                        puntero = puntero.izquierdo;
                    }
                }
            }
            Console.WriteLine("No se encontró historia clínica");
            Console.WriteLine("Total de iteraciones:" + contador);
        }
    }
}
