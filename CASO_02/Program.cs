﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CASO_02
{
    class Program
    {
        static void Main(string[] args)
        {
            Arbol historias = new Arbol();
            Arbol historias2 = new Arbol();
            historias.InsertarNodo(5, "Historia 5", "Paciente 5");
            historias.InsertarNodo(1, "Historia 1", "Paciente 1");
            historias.InsertarNodo(6, "Historia 6", "Paciente 6");
            historias.InsertarNodo(10, "Historia 10", "Paciente 10");
            historias.InsertarNodo(11, "Historia 11", "Paciente 10");
            historias.InsertarNodo(15, "Historia 15", "Paciente 15");

            historias.BuscarPorID(15);

            Console.WriteLine("\nOrdenando en forma balanceada (mas eficiente):\n");

            historias2.InsertarNodo(6, "Historia 6", "Paciente 6");
            historias2.InsertarNodo(5, "Historia 5", "Paciente 5");
            historias2.InsertarNodo(1, "Historia 1", "Paciente 1");
            historias2.InsertarNodo(11, "Historia 11", "Paciente 11");
            historias2.InsertarNodo(10, "Historia 10", "Paciente 10");
            historias2.InsertarNodo(15, "Historia 15", "Paciente 15");

            historias2.BuscarPorID(15);

            Console.Read();    
        }
    }
}
