﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET3
{
    class Nodo
    {
        public int id;
        public string nombre, apellido, carrera;
        public Nodo derecho, izquierdo;
    }
}
