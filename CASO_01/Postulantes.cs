﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET3
{
    class Arbol
    {
        public Nodo raiz;
        public Nodo GetRaiz()
        {
            return raiz;
        }
        public void InsertarNodo(int id, string nombre, string apellido, string carrera)
        {
            Console.WriteLine("Almacenando informacion de {0} {1} con ID: {2}",nombre,apellido,id);
            Nodo puntero;
            Nodo padre;
            Nodo nodo = new Nodo {id = id, nombre = nombre, apellido = apellido, carrera = carrera};
            if (raiz != null)
            {
                puntero = raiz;
                while (true)
                {
                    padre = puntero;
                    if (id < puntero.id)
                    {
                        puntero = puntero.izquierdo;
                        if (puntero == null)
                        {
                            padre.izquierdo = nodo;
                            break;
                        }
                    }
                    else
                    {
                        puntero = puntero.derecho;
                        if (puntero == null)
                        {
                            padre.derecho = nodo;
                            break;
                        }
                    }
                }
            }
            else
            {
                raiz = nodo;
            }
        }
        public void Inorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Inorden(raiz.izquierdo);
                Console.WriteLine("ID: {0}, {1} {2}, ", raiz.id, raiz.nombre, raiz.apellido, raiz.carrera);
                Inorden(raiz.derecho);
            }
        }

        public void Preorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Console.WriteLine("ID: {0}, {1} {2}, ", raiz.id, raiz.nombre, raiz.apellido, raiz.carrera);
                Preorden(raiz.izquierdo);
                Preorden(raiz.derecho);
            }
        }

        public void Postorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Postorden(raiz.izquierdo);
                Postorden(raiz.derecho);
                Console.WriteLine("ID: {0}, {1} {2}, ", raiz.id, raiz.nombre, raiz.apellido, raiz.carrera);
            }
        }

    }
}
