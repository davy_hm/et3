﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET3
{
    class Program
    {
        static void Main(string[] args)
        {
            Arbol Postulantes = new Arbol();
            Postulantes.InsertarNodo(1, "Juan", "Perez", "Ingeniería de Sistemas");
            Postulantes.InsertarNodo(6, "Carlos", "Rojaz", "Administración y Finanzas");
            Postulantes.InsertarNodo(5, "Hugo", "Saenz", "Contabilidad");
            Postulantes.InsertarNodo(10, "Pedro", "Motta", "Ingenieria Industrial");
            Postulantes.InsertarNodo(9, "Alex", "Hidalgo", "Ingenieria Electrónica");

            Console.WriteLine("\nLista de postulantes en INORDEN:\n-------------------------------");
            Postulantes.Inorden(Postulantes.GetRaiz());

            Console.WriteLine("\nLista de postulantes en PREORDEN:\n--------------------------------");
            Postulantes.Preorden(Postulantes.GetRaiz());

            Console.WriteLine("\nLista de postulantes en POSTORDEN:\n---------------------------------");
            Postulantes.Postorden(Postulantes.GetRaiz());


            Console.Read();
        }
    }
}
